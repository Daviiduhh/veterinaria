import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Login.vue"),
  },
  {
    path: "/registro",
    name: "Registro",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Registro.vue"),
  },
  {
    path: "/registro/cuenta",
    name: "RegistroCuenta",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/RegistroCuenta.vue"),
  },
  {
    path: "/informacion",
    name: "Informacion",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Informacion.vue"),
  },
  {
    path: "/mascotas",
    name: "Mascotas",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Mascotas.vue"),
  },
  {
    path: "/mascotas/detalles",
    name: "Detalles",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/MascotasDetalles.vue"),
  },
  {
    path: "/mascotas/agregar",
    name: "Agregar",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Agregar.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
